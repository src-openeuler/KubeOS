# Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.

Name:           KubeOS
Version:        1.0.8
Release:        5
Summary:        O&M platform used to update the whole OS as an entirety
License:        Mulan PSL v2
Source0:        https://gitee.com/openeuler/KubeOS/repository/archive/v%{version}.tar.gz
Patch1:         0001-fix-kbimg-add-return-statement-for-non-existent-disk.patch
Patch2:         0002-fix-kbimg-incorrect-remove-chroot-script-path.patch
Patch3:         0003-fix-admin-container-correct-path-for-copying-files-i.patch
Patch4:         0004-fix-kbimg-add-info-log-for-successful-image-creation.patch
Patch5:         0005-fix-kbimg-enforce-non-empty-strings-for-required-fie.patch
Patch6:         0006-fix-kbimg-add-validation-for-non-empty-strings-in-Ve.patch
Patch7:         0007-fix-kbimg-set-default-umask-to-022.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-build
BuildRequires:  make rust cargo openssl-devel
BuildRequires:  golang >= 1.13

%description
This is an O&M platform used to update the whole OS as an entirety,
it should be running in kubernetes environment.

%prep
%autosetup -n %{name}-v%{version} -p1

%package scripts
Summary: Scripts to build the os image and binaries of os-proxy and os-operator
Requires: qemu-img, parted, bc, tar, docker, dosfstools
%description scripts
The scripts package includes scripts which could build the os image and binaries of os-proxy and os-operator

%define debug_package %{nil}
%define __debug_install_post \
%{_rpmconfigdir}/find-debuginfo.sh %{?_find_debuginfo_opts} "%{_builddir}/%{?buildsubdir}" \
%{nil}

%build
mkdir ./.cargo
cat << EOF >> ./.cargo/config.toml

[source.crates-io]
replace-with = "vendored-sources"

[source.vendored-sources]
directory = "KubeOS-Rust/vendor"
EOF

make

%install
install -d %{buildroot}%{_bindir}
#install binary
install -d -m 0740 %{buildroot}/opt/kubeOS/bin
install -p -m 0500 ./bin/rust/release/os-agent %{buildroot}/opt/kubeOS/bin
install -p -m 0500 ./bin/rust/release/proxy %{buildroot}/opt/kubeOS/bin
install -p -m 0500 ./bin/operator %{buildroot}/opt/kubeOS/bin
install -d -m 0740 %{buildroot}/opt/kubeOS/scripts
install -p -m 0500 ./bin/rust/release/kbimg %{buildroot}/opt/kubeOS/scripts

#install artifacts
install -p -m 0600 ./KubeOS-Rust/kbimg/kbimg.toml %{buildroot}/opt/kubeOS/scripts

%files
%defattr(-,root,root,0500)
%attr(0500,root,root) /opt/kubeOS/bin/os-agent

%files scripts
%attr(0500,root,root) /opt/kubeOS/bin/proxy
%attr(0500,root,root) /opt/kubeOS/bin/operator
%defattr(-,root,root,0500)
%attr(0600,root,root) /opt/kubeOS/scripts/kbimg.toml
%attr(0500,root,root) /opt/kubeOS/scripts/kbimg

%clean
rm -rfv %{buildroot}

%changelog
* Tue Mar 11 2025 Yuhang Wei<weiyuhang3@huawei.com> - 1.0.8-5
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:set default umask to 022 for KubeOS image creation

* Mon Dec 16 2024 Yuhang Wei<weiyuhang3@huawei.com> - 1.0.8-4
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:enforce non-empty vec strings for required fields

* Fri Dec 13 2024 Yuhang Wei<weiyuhang3@huawei.com> - 1.0.8-3
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:enforce non-empty strings for required fields

* Wed Dec 11 2024 Yuhang Wei<weiyuhang3@huawei.com> - 1.0.8-2
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:bugs fix of pxe and admin-container

* Tue Dec 10 2024 Yuhang Wei<weiyuhang3@huawei.com> - 1.0.8-1
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:update version to 1.0.8

* Tue Jun 11 2024 Yuhang Wei<weiyuhang3@huawei.com> - 1.0.6-2
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:modify version display and fix the path for EFI boot in bootloader.sh

* Mon Apr 01 2024 Yuhang Wei<weiyuhang3@huawei.com> - 1.0.6-1
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:update version to 1.0.6-1

* Fri Feb 23 2024 Yuhang Wei<weiyuhang3@huawei.com> - 1.0.5-4
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:fix code check

* Mon Feb 05 2024 Yuhang Wei<weiyuhang3@huawei.com> - 1.0.5-3
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:bump tokio to 1.28.0

* Mon Jan 29 2024 Yuhang Wei<weiyuhang3@huawei.com> - 1.0.5-2
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:sync code from source master branch

* Wed Jan 10 2024 Yuhang Wei<weiyuhang3@huawei.com> - 1.0.5-1
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:update version to 1.0.5.1

* Tue Sep 05 2023 Yuhang Wei<weiyuhang3@huawei.com> - 1.0.4-4
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:fix proxy requeue and update image label

* Thu Aug 24 2023 Yuhang Wei<weiyuhang3@huawei.com> - 1.0.4-3
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:sync code from source master branch

* Mon Aug 07 2023 liyuanrong<liyuanrong1@huawei.com> - 1.0.4-2
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:fix the hostshell cannot obtain the lib

* Wed Aug 02 2023 liyuanrong<liyuanrong1@huawei.com> - 1.0.4-1
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:update version to 1.0.4.1

* Thu Dec 08 2022 liyuanrong<liyuanrong1@huawei.com> - 1.0.2-9
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:sync code from source, support containerd upgrade,settings and admin-container

* Thu Dec 08 2022 liyuanrong<liyuanrong1@huawei.com> - 1.0.2-8
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:fix usage does not print when an error occurs in the upgrade image creation

* Tue Nov 29 2022 liyuanrong<liyuanrong1@huawei.com> - 1.0.2-7
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:remove grub2 legacy install, add error handling for opstype and add entry for unit test in Makefile

* Sat Sep 03 2022 liyuanrong<liyuanrong1@huawei.com> - 1.0.2-6
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:add the configuration of /etc/resolv.conf and change the VM disk to gpt.

* Wed Aug 31 2022 liyuanrong<liyuanrong1@huawei.com> - 1.0.2-5
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:add the clearing of space before the upgrade and rectifying the rollback failure.

* Mon Aug 29 2022 liyuanrong<liyuanrong1@huawei.com> - 1.0.2-4
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:fixed the issue of VMs images and add check of Global.cfg.

* Tue Aug 23 2022 liyuanrong<liyuanrong1@huawei.com> - 1.0.2-3
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:fix the kbimg.sh exception and pxe installation

* Fri Aug 05 2022 liyuanrong<liyuanrong1@huawei.com> - 1.0.2-2
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:update to 1.0.2-2

* Tue Aug 02 2022 liyuanrong<liyuanrong1@huawei.com> - 1.0.1-8
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:update to 1.0.1-8

* Fri Dec 17 2021 liyuanrong<liyuanrong1@huawei.com> - 1.0.1-5
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:add arm architecture support to the OS image

* Wed Dec 08 2021 linxiaoxu<linxiaoxu@huawei.com> - 1.0.1-4
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:fix bugs of change generate argument from isopath to repopath

* Thu Nov 11 2021 liyuanrong<liyuanrong1@huawei.com> - 1.0.1-3
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:fix bugs of checks in generate.sh and change module path

* Sat Oct 30 2021 liyuanrong<liyuanrong1@huawei.com> - 1.0.1-2
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:update compressed package

* Fri Oct 29 2021 linxiaoxu<linxiaoxu@huawei.com> - 1.0.1-1
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:update version to v1.0.1

* Tue Oct 19 2021 linxiaoxu<linxiaoxu@huawei.com> - 1.0.0-4
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:change argument check range

* Thu Sep 30 2021 liyuanrong<liyuanrong1@huawei.com> - 1.0.0-2
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:update spec

* Thu Sep 30 2021 liyuanrong<liyuanrong1@huawei.com> - 1.0.0-2
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:add Arch to Spec

* Thu Sep 30 2021 liyuanrong<liyuanrong1@huawei.com> - 1.0.0-1
- Type:requirement
- CVE:NA
- SUG:restart
- DESC:First release KubeOS in rpm package
